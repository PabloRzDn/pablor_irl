import { CssBaseline, ThemeProvider } from '@mui/material'
import { theme } from "./theme.config"

export const ThemeProviderConfig = ({children}: any) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline/>
      {children}
    </ThemeProvider>
  )
}

