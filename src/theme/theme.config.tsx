import { createTheme } from "@mui/material";
import { lightThemePalette } from "./theme.models";

export const theme = createTheme({

  palette: {
    mode: 'light',
    background: {
      default: lightThemePalette.BACKGROUND
    },
    primary: {
      main: lightThemePalette.NAVBAR
    },

  },
  typography: {
    fontFamily: "'Ubuntu Mono', monospace",
    
  },


  components: {
    MuiCssBaseline: {
      //styleOverrides: {
      //  body: {
      //    scrollbarColor: "#6b6b6b #2b2b2b",
      //    "&::-webkit-scrollbar, & *::-webkit-scrollbar": {
      //      backgroundColor: "#2b2b2b",
      //    },
      //    "&::-webkit-scrollbar-thumb, & *::-webkit-scrollbar-thumb": {
      //      borderRadius: 8,
      //      backgroundColor: "#6b6b6b",
      //      minHeight: 24,
      //      border: "3px solid #2b2b2b",
      //    },
      //    "&::-webkit-scrollbar-thumb:focus, & *::-webkit-scrollbar-thumb:focus": {
      //      backgroundColor: "#959595",
      //    },
      //    "&::-webkit-scrollbar-thumb:active, & *::-webkit-scrollbar-thumb:active": {
      //      backgroundColor: "#959595",
      //    },
      //    "&::-webkit-scrollbar-thumb:hover, & *::-webkit-scrollbar-thumb:hover": {
      //      backgroundColor: "#959595",
      //    },
      //    "&::-webkit-scrollbar-corner, & *::-webkit-scrollbar-corner": {
      //      backgroundColor: "#2b2b2b",
      //    },
      //  },
      //},
    },
    MuiCard:{
      styleOverrides:{
        root:{
          backgroundColor: "#526D82"
        }
      }
    },
    MuiCardActions:{
      styleOverrides:{
        root:{
          backgroundColor: "#526D82"
        }
      }
    },
    MuiTypography: {
      styleOverrides: {
        h5: {
          color:"#DDE6ED",
          fontSize: '20px'   
        },
        body1: {
          color:"#9DB2BF",
          fontSize: '15px'   
        },
        body2: {
          color:"#DDE6ED",
          fontSize: '15px'   
        },
      },
    },
    MuiDialog:{
      styleOverrides:{
        paper:{
          backgroundColor: "#151D29"
        }
      }
    },
    MuiTab: {
      styleOverrides: {
        
        textColorPrimary: '#E6F0F3', // Color blanco invierno
        },
      },
    
  },
})