import './App.css';
import Layout from './common/Layout/Layout.component';
import Main from './common/Main/Main.component';

function App() {
  return (
    <div className="App">
      <Layout>
       <Main/>
      </Layout>
    </div>
  );
}

export default App;
