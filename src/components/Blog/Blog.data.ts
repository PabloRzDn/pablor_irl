export const blogData = [
  {
    id: 1,
    content: `
    Todavía no he admitido la derrota,
    ni tampoco me he retirado:
    esa alta inspiración,
    ese talento con el cual fui provisto,
    no ha sido descartado.
    La capacidad para llenar con leche de camello
    los toneles no ha disminuido -
    aparte de mi pausa deliberada,
    no existe diferencia en mi.
    Así que tengo algunas cuestiones que plantear
    para hacer frente a los divulgadores de la duda.

    Cuando los hombres se entregan a la lucha
    y se determinan a cumplir con su deber;
    cuando se preparan para la embestida, 
    reúnen los más finos purasangres;
    cuando las riendas están en los jinetes,
    nunca me hago a un lado.

    Cuando nuestro debate se acalora
    los pretextos salen a la luz:
    ese daño causado por la explotación
    la amargura de las tribus, la angustia de
    los clanes,
    que abre heridas tan profundas.
    divisiones entre el pueblo.
    Cuando las hienas descienden
    a las tumbas superficiales 
    y los cuerpos rechazados de los muertos,
    esa carne podrida que nada más podría tocar,
    y arrancan la pulpa de sus vientres,
    el flujo y las flemas, 
    y los esparcen por todas partes,
    el pueblo se pasma,
    el aire fresco se infecta,
    el hedor llena cada orificio nasal.
    Entonces soy esa predicción
    de nubes por llegar
    trayendo un aguacero
    que cubrirá toda la nación.
   
    [...]
   
    Cualquiera que intente robar tus derechos,
    sea por el hurto descarado
    o la cleptocracia clandestina, 
    con espantosas artimañas o con
    dedos ligeros a plena luz del día,
    el más mezquino de los hurtos egoístas
    no pueden comprender lo bien fundada que es la Libertad - 
    tanto la Luna y toda su claridad
    que nunca se oscurecerá.
    Cargo su fuego, y soy
    su emisario. Cualquiera
    que busque hacerme daño,
    o encontrar mi punto más débil, 
    puede hacer todo lo posible: 
    no importa cuán agotado parezca, 
    la Historia mostrará
    en los registros de la nación
    el resultado inevitable 
    del papel que desempeño.
      
    [...]
      
    Que estas líneas sean tan notables
    como las rayas sobre el oryx, 
    tan visibles y tan bellas,
    que simplemente las pongo a la vista.
    Pero hay un punto más
    que terminará mi argumento.
    Aquellos poemas de los otros son impulsados por el viento,
    giran como un tornado
    donde sea y en la dirección que sea: 
    enarbolando la bandera de la tribu,
    levantando su lanza mortal,
    quitando la cubierta de la restricción 
    desplegada en todos los mercados.
    ¿No existe regulación que lo impida,
    ninguna ley que los pueda detener,
    ninguna autoridad que lo haga cumplir?
    Me pregunto ¿quién dijo que dejáramos extender
    esta malaria tan funesta?
    ¿por qué a quienes los elogian 
    no han sido llevados a juicio?
      
    [...]
      
    Quién sabe cuál es la manera de ordenar
    el dibujo del agua pero tú, Gaarriye,
    dominar con destreza para distinguir
    hojas secas de la yesca y palos muertos
    del tronco y las mejores ramas para el fuego.
    Eres acero templado:
    Tu discurso satisface la curiosidad
    y tus argumentos convencen.
    Tu caballo es nombrado Doolaal, el fuerte y veloz:
    deja que su relincho vuelva a resonar,
    libera sus riendas, deja 
    que nuestro tema se extienda a lo lejos.
    Querido amigo, Maxamed Xaashi,
    soy como tú y hago lo que haces,
    si alguna vez te decepciono,
    deja que la culpa caiga sobre mi. 
    Estemos unidos en nuestro objetivo común:
    claro de motivo,
    claro de voz.
    `,
  },
  {
    id: 2,
    content: `
    Agosto
    
    Cuando las moras cuelgan
    hinchadas en el bosque, en las zarzas 
    que son de nadie, paso   
    
    todo el día entre las altas
    ramas, alcanzando
    mis brazos rasgados, pensando
    
    en nada, rebosando 
    la miel negra del verano
    dentro de mi boca; todo el día mi cuerpo
    
    acepta lo que es. En la oscuridad
    acequias que corren por allí es
    esta gruesa pata de mi vida disparada entre 
    
    las campanas negras, las hojas: ahí existe 
    esta lengua feliz

    -----------

    Hongos

    LLueve, y entonces
    los helados y fruncidos
    labios del viento
    los sacan
    de la tierra - 
    calaveras rojas y amarillas
    golpeando hacia arriba
    a través de las hojas, 
    a través de la hierba,
    a través de la arena; asombradas
    por su rudeza,
    su quietud,
    su humedad, aparecen
    en mañanas otoñales, algunos
    balanceándose en la tierra
    sobre una pezuña
    repletos de veneno,
    otros se agitan
    en pedazos, y gustosos - 
    aquellos que saben
    salir a recolectar, eligiendo
    los benignos de entre 
    los brillantes,  hechiceros
    russulas,
    sombreros de pantera,
    ángeles de la muerte tiburón blanco
    en sus velos rasgados
    inocentes como el azúcar 
    pero repletos de parálisis:
    comer
    es tambalearse 
    rápido como los mismos hongos
    cuando acaban de ser perfectos
    y de la noche a la mañana
    se deslicen bajo los brillantes
    campos de lluvia

    -----------

    Gatito

    Más asombrada que nada
    tome al gatito negro
    nacido muerto
    con un gran ojo
    en el centro de su pequeña frente
    desde la cama del gato
    y lo enterré en el patio
    tras la casa

    Supongo que podría haberlo ofrecido
    al museo,
    podría haber llamado 
    al periódico local.

    Pero en vez de eso lo saqué al patio
    y abrí la tierra
    y lo puse en su sitio
    diciendo, esto fue real,
    diciendo, la vida es infinitamente creativa, 
    diciendo que otra espantosa
    mentira en la oscura semilla de la tierra, sí,

    pienso que hice lo correcto en salir sola
    y darle su pacífico lugar, y cubrir el lugar 
    con el florecer insolente de la hierba


    ------------

    Canción de otoño

Otro año se ha ido, dejando en todas partes
sus ricos despojos especiados: vinos, hojas,

los frutos no comidos desmoronándose húmedos
en las sombras, nimio volver

desde la particular isla
de este verano, este Ahora, ese ahora es ninguna parte

excepto bajo nuestros pies,  desmoronándose
en ese castillo negro y subterráneo

de misterios inobservables - raíces y semillas selladas
y las peripecias del agua. Esto

es lo que trato de recordar cuando la medida del tiempo
irrita dolorosamente, por ejemplo cuando el otoño 

al final se enciende, bullicioso y como nosotros ansiando
permanecer - como todo lo vivo, pasando

-----------

En el bosque de pinos, cuervos y lechuzas 

Gran vacilación. Pulcro
segador. La forma en que los cuervos
sueñan contigo, atrapada por fin
en sus picos. Sueñan contigo
perdiendo la vida. Tus alas
desmoronándose como viejas 
cortezas. Plumas
cayendo de tu pecho como hojas,
y tus ojos dos cerrojos
de los rayos dormidos.
Ocho de ellos
vuelan sobre el bosque de pinos mirando hacia abajo
por entre las ramas. Saben que estás ahí
en alguna parte, gorda y somnolienta
de tu noche de conejos y ratas. Una vez
durante este mes has atrapado un cuervo. Sus pedazos
volaron por todas partes, las noticias
resonaron todo el día por el bosque. el gélido 
río de su odio
dia y noche; eres su sueño, su vigilia,
su presa, su demonio. Tú
eres el dios del pino, quien nunca habla pero domina
las llaves de todo mientras ellos vuelan
mañana tras mañana contra las puertas cerradas. Tú
tendrás una vida tranquila, y te alimentarás de ellos, uno tras otro.
Ellos lo saben. Ellos te odian. Aun así, 
cuando uno de ellos te vigile, todo concurre
directo a la violencia y la confrontación.
Como si ayudara a ver la prueba en vida.
El príncipe que tritura los huesos de los días oscuros, sombrío
ante la interrupción de su descanso. Siseando
y crujiendo, aferrándose a él, terrible
como los tambores de la muerte. lúgubre, un hecho inmutable.

    `,
  },
  {
    id: 3,
    content: `
    En el principio (en el principio del tiempo para no decir
    algo más)  había brújulas: danzando en el 
    vacío con sus pies han descubierto los principios y finales,
    principio y final en una sola linea, la sabiduría giraba
    también en círculos, para aquellos donde su reino: el sol
    los mundos giraron,  llegaron las estaciones, mas 
    todas las cosas siguieron su curso: pero en el principio
    principio y final fueron uno solo.
    
    Y en el principio estaba el amor. Amor hecho una esfera:
    todas las cosas crecieron en su interior; la esfera entonces
    contenía principios y finales, principio y final. El amor
    tuvo entonces una brújula cuya danza giratoria trazaba
    una esfera de amor en medio del vacío: en el centro de si misma
    brotaba una fuente.

    -----------

    Potreros preparados
    para el circo,
    artistas para los estelares,
    antes que todo
    un elefante de carga
    o una carpa que se alza. 

    -----------

    En silencio el potrero esperaba;
    fue bendecido con la maravilla de la creación.
    Trabajadores han llegado de otros mundos; como ángeles visitadores,
    Ellos hablan en vernáculo y evaden las preguntas 
    con bromas: sus pantalones roñosos, camisas de mezclilla azul, carne roja
    de los elementos;
    Sus ojos miran profundamente atrás, infinitamente más allá. Penetran
    pero no aprecian:
    contemplando todas las cosas antes de ellos con la inocencia de la luz.
    Visitantes extraños, cuando ellos se reúnen
    mueren de la risa,
    juntos fulgen sus miradas como el agua bajo la luz del sol.

    Son los elegidos para estirar las cuerdas y armar la carpa;
    tramoyas con sus gritos y martillos, clavan las estacas que  
    mantienen en su lugar al ondulante firmamento.

    Bagonghi dice, “tomaré tu traje hasta que Mogador
    despierte.”

    Enano, de piernas arqueadas, se contorsiona de lado a lado, un tirón 
    y en un solo movimiento ha cruzado el lugar,
    sosteniendo su capacho a centímetros del suelo.
    Abre la puerta más ancha del trailer, se para de puntillas,
    gira el interruptor en medio de la oscuridad. “Estará listo para cuando 
    Mogador despierte”.
    Vuelve, dejando la puerta entreabierta.

    La tierra del potrero es rica y fértil, pero ¿quién
    se alimentará de este pasto? Caballos, camellos, cebras. 
        `,
  },
  {
    id: 4,
    content: `
    Llanura        silencios 
    del Chaco    siluetas 
    y ensayos del cuerpo

    discos de piedra
    		las manos
    		macanas 
    		lóbulo horadado

    del cedro se hizo el arco
    al fuego somete su curva
    tallar una obsidiana en la cavidad
    de una herida  
                         quebracho
    	        veneno
    langostas crepitan
    y un camino
    se interna hacia el monte

    los ojos de la tribu en las estrellas
    la noche un tiempo de animales
    chajá   biguá   
                           ñandú
    la caza no es disputa   sino
    respirar el mismo lenguaje
    reverencias
                    el olfato de la especie 

    ----------

    Al margen de Kalasasaya
    se yergue un portal monolítico
    divinidad sobre relieve por donde 
    cabe el cosmos completo
    Al margen del pilar 
    de un portal monolítico 
    se esculpe la forma de un portal monolítico que se yergue 
    y desdibuja el trazo de una divinidad en el dintel

    A la vera
    de otro templo inscrito 
    en la tierra hay una maqueta tallada en piedra de un templo inscrito
    							en la tierra
    Ciento veintisiete rostros tiwanacotas y tres 
    estemas menores concurren en el centro
    Ojos de caliza
    tierra apisonada y siete escalones
    concurren en el centro 

    La noche es un templo lítico
    donde el cosmos concurre 
    en la tierra y su propio
                centro 


    ----------


    Chandra cruza un puente que se mece
    la multitud empuja un barco con pañuelos
    una trayectoria recta sobre el globo de la sala de control
    Mumbai
    		Venecia
    es una geodésica imposible
    estirar un cordón sobre mares
    y arrozales abandonados

    La estela asimétrica que deja la embarcación en su viraje
    se alarga eternamente hasta las algas de los manglares
    					encuentra un muro de hielo

    La recámara es un lugar para pensar y piensa que así
    como las tácticas de caza y los mapas rudimentarios
    los números y letras primero fueron surcos en el barro
    una forma suficiente y lograda secándose en el sol
    ganado enfermo del imperio
    navíos confiscados de una guerra
    el peso de una estrella que oscila en la punta de una cuchara
    la sombra de las cosas proyectándose en el volumen de los remaches
    dislocaciones del acero al contacto con el agua
    olas rompen sobre el casco

    -----------

    aparición del árbol
    y los años secos que iluminan el interior 
    de la caverna    el esbozo de un volcán con respecto 

    a los arpones     la pradera     verano de los búfalos 
    y el ocre deja de humectar la historia para agradecer
    un momento         pues el suelo ha sido generoso 

    tierra       que arroja hacia el mundo el magma
    y los grupos humanos     apuntan la imagen    pómez
    grietas y el penacho incandescente   

    proyectado en la diagonal del habitáculo 
    y los lugares se vuelven ahora más precisos
    al tiempo en que la arcilla es el esbozo de la piedra

    un volcán   el violáceo ventisquero
    hielo     azul verdoso y la sierra
    negra       el contorno de la noche reflejada sobre el cráter  


    -----------

    Mar Rojo
    como las lineas del oryx
    los rayos del sol	 
    luego el golfo y el cuerno de arena
    sobre el horizonte
    la fuente de las nubes
    que Hadraawi simplemente
    pone a la vista

    Mercaderes han dicho ante la imagen
    aquella es una nación de poetas
    un jefe indica un gabay frente a su aldea
    un clan que canta
    un río de sangre que se detiene
    que han sido declamados
    los códigos morales
    las leyes

    Chandra piensa ante la imagen
    poder decir acaso lo que vemos 
    una extraña combinación
    de ondas y partículas
    iridiscencia de las charonias en el mar que se recoge
    Las reglas cuánticas
    describir las propiedades de las playas
    sin llegar a distinguir los granos de arena

    -----------


    En el gran salón
    han traído el corolario
    a un terreno conocido

    Ser igual a la variación de lo que eres

    Los vestigios de un monzón afectan 
    la gravedad de los castaños 
    limo 	semillas
                 banalidad de un cráter 

    en la mente de Lalitha espirales
    la forma omnipresente
    hélices de un vapor 
    		hendiendo agua salada
    sumideros del océano  
    el universo completo en vibración

    Antes de partir
    Chandra siempre dijo
    que Euler lo asombraba


    ------------

    La lluvia
    rogativa sedienta de los pavos y el canto 
    			del pájaro koel 
    como Mirabai y su deseo
    		todo queda atrás

    Lalitha y los vagones a Chennay
    cautiverio ante la inercia del mundo

    sobre el mesón del camarote apunta
    las ecuaciones del movimiento
    			un nevado y su reposo
    recuerda el afán 
    	de Chandra por lo inteligible
    medir la velocidad de un convoy 
    por las figuras del té derramado en los mendrugos
    el rastro irregular de las letrinas sobre los durmientes 
    silencio de un tractor sobre tierras inundadas
    onda expansiva de las gotas que despiertan al germen del arroz
    					la región visible del espectro


    ------------

    otoño 
    y la distancia se han cubierto
    de numerosas distracciones
    
    Lalitha insiste 
    en el tañir de la veena 
    coplas que arrullan 
    la luna y el amor 
    su lado oculto 
    especulación
    tan opaca y bella
    como en la noche de bangalore
    sus sonidos y silencios
    
    Chandra y el amor
    animal menguante
    aquella superficie oscura
    colmada de tanto daño
    

    ------------

    Observatorio de rayos X Chandra

    Las observaciones
    bajo cierto tipo de espectrometría
    dicen que la forma de los planetas jovinos
    solo tienen sentido desde fuera
    La densidad de los gases 
    metal líquido
    la esfera
    no se perturban por los asteroides
    ni erupciones de ningún tipo
      
    Kabir dice que el río y las olas
    forman una misma superficie
    que hay astros que viven en el interior
    pero los ojos no pueden ver
      
    La sonda Chandra mide la abundancia
    de los elementos tal como el marabú contempla
    el perfil de los caudales
    la agitación del líquido bajo el vaho
    cenizas
    Los desagües del Yamuna no contienen
    la podredumbre de las riveras





        `,
  },
];
