import {
  Card,
  CardActions,
  CardContent,
  Divider,
  Grid,
  Typography
} from "@mui/material";

import BlogModal from "../Modals/BlogModal.component";
interface cardContentInterface {
  id: number;
  title: string;
  subtitle: string;
}

const cardContent: cardContentInterface[] = [
  {
    id: 1,
    title: "Maxamed Ibraahin Warsame 'Hadraawi' - Traducción del inglés de Claridad' (fragmento - sin editar)",
    subtitle:
      `02/03/2024 - Hadraawi (traducido al castellano como el maestro del discurso) nace en el seno de una familia nómada a las afueras de Burco, 
      la segunda ciudad más grande de la Somalilandia Británica, en tiempos álgidos de resurgimientos nacionalistas y demandas de libertad frente a las potencias coloniales de la época. 
      Se convierte en profesor, dónde comienza su acercamiento a la poesía.  En respuesta a la opresión del régimen militar en Somalía que, entre otras cosas, prohibió oficialmente el tribalismo 
      y creó órganos represivos para mantener el control local, Hadraawi y Mahamed Haashi Dhamac "Gaariye" desafiaron al régimen liderando la famosa cadena poética denominada Deelley,
       expresando así los sentimientos del pueblo frente a la realidad política y social del país.`
  },
  {
    id: 2,
    title: "Mary Oliver: Traducciones de 'Américan Primitive'(sin editar)",
    subtitle:
      `21/01/2024 - Nacida y criada en Maple Hills Heights, un suburbio de Cleveland, Ohio el año 1935. 
      Su trabajo recibió numerosos premios, incluido el Premio Pulitzer, el National Book Award y un Premio Literario Lannan por el logro de toda una vida.
      Su colección "American Primitive" ganó el Pulitzer y estableció a Oliver como una de las mejores poetas estadounidenses. En obras posteriores como "Dream Work" y "New and Selected Poems", Oliver exploró la 
      maravilla y el dolor de la naturaleza, así como los reinos más personales. Mary Oliver fue también una prolífica escritora de prosa y poesía, publicando regularmente. Su enfoque principal fue
       la intersección entre lo humano y lo natural, así como los límites de la conciencia y el lenguaje humanos para articular tal encuentro. `
  },
  {
    id: 3,
    title: "Robert Lax: Traducciones de 'Circus of the sun' (sin editar)",
    subtitle:
      `21/09/2023 - Robert Lax nació en Olean, Nueva York, y fue educado en la Universidad de Columbia. Trabajó como editor para el New Yorker, Jubilee y PAX.
      Entre sus numerosas colecciones de poesía se encuentran "The Circus of the Sun" (1959), "New Poems" (1962) y "Dark Earth Bright Sky" (1985). 
      Su poesía también fue incluida en antologías como "Concrete Poetry: An International Anthology" (1967, editada por Stephen Bann) y "Concrete Poetry: 
      A World View" (1968, editada por Mary Ellen Solt). Una visión general de su obra se encuentra en "Poems (1962–1997)" (2013, editada por John Beer).
      Lax se convirtió al catolicismo en 1943 y buscando una vida de claridad espiritual y compromiso artístico, se mudó a Grecia en 1962, estableciéndose primero en la isla de Kalymnos y luego en la isla de Patmos, donde vivió por más de 30 años. 
      Regresó a su ciudad natal, Olean, Nueva York, en las últimas semanas de su vida.`
  },
  {
    id: 4,
    title: "De talleres",
    subtitle:
      "01/11/2023 - Colección de algunos textos inéditos trabajados en talleres literarios",
  },
];

const Blog = () => {
  return (
    <>
      <Grid container spacing={1}>
        {cardContent.map((item: cardContentInterface) => (
          <Grid key={`${item.id}_grid`} item xs={12} xl={12}>
            <Card key={`${item.id}_card`}>
              <CardContent key={`${item.id}_content`}>
                <Typography gutterBottom variant="h5" component="div">
                  {item.title}
                </Typography>
                <Typography variant="body2" color="text.secondary">
                  {item.subtitle}
                </Typography>
              </CardContent>
              <Divider></Divider>
              <CardActions key={`${item.id}_actions`}>
                
                  <BlogModal 
                  id={item.id}
                  title={item.title}
                  subtitle={item.subtitle} />
                
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default Blog;
