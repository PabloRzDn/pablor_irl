import {
  ImageList,
  ImageListItem,
  ImageListItemBar
} from '@mui/material';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import PetModal from '../Modals/PetModal.component';
import { petList } from './Pets.data';

const Pets = () => {
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));


  return (

    <ImageList 
    cols={3} 
    rowHeight={fullScreen? 120 : 250}
    variant="quilted" 
    sx={{ width: "100vh", height: "55vh" }}>
      
      {petList.map((item) => (
        <ImageListItem key={item.img}>
          <img
            src={`${item.img}?w=248&fit=crop&auto=format`}
            srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
            alt={item.title}
            loading="lazy"
          />
          <ImageListItemBar
            title={item.title}
            subtitle={item.author}
            actionIcon={
              <PetModal pet={item}/>
            }
          />
        </ImageListItem>
      ))}
    </ImageList>
  )
}

export default Pets