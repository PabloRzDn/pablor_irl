
export const petList = [
    {
        id: 1,
        img: require("./assets/naidora001.jpg"),
        title: "Dora&Nai",
        author: ":3"
    },
    {
        id: 2,
        img: require("./assets/naidora002.jpg"),
        title: "Dora&Nai",
        author: "mimiendo"
    },
    {
        id: 3,
        img: require("./assets/naidora003.jpg"),
        title: "Dora&Nai",
        author: "arbolito"
    },
    {
        id: 4,
        img: require("./assets/naidora004.jpg"),
        title: "Nai",
        author: "Cazuela"
    },
    {
        id: 5,
        img: require("./assets/naidora005.jpg"),
        title: "Nai",
        author: ":3"
    },
    {
        id: 6,
        img: require("./assets/naidora006.jpg"),
        title: "Nai",
        author: ":3"
    },
    {
        id: 7,
        img: require("./assets/naidora007.jpg"),
        title: "Nai",
        author: ":3"
    },
    {
        id: 8,
        img: require("./assets/naidora008.jpg"),
        title: "Dora",
        author: ":3"
    },
    {
        id: 9,
        img: require("./assets/naidora009.jpg"),
        title: "Nai&Dora",
        author: ":3"
    },
    {
        id: 10,
        img: require("./assets/naidora010.jpg"),
        title: "Nai",
        author: "wawita"
    },
    {
        id: 11,
        img: require("./assets/naidora011.jpg"),
        title: "Nai",
        author: "Cazuela"
    },

]