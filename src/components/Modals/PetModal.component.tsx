import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import InfoIcon from '@mui/icons-material/Info';
import { IconButton, Typography } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useState } from 'react';

const PetModal = ({ pet }: any) => {
    const theme = useTheme();

    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));


    const [openModal, setOpenModal] = useState(false)
    const handleOpen = () => {
        setOpenModal(true)
    }
    const handleClose = () => {
        setOpenModal(false)
    }
    return (
        <div>
            <IconButton
                onClick={handleOpen}
                sx={{ color: 'rgba(255, 255, 255, 0.54)' }}
                aria-label={`info about ${pet?.title}`}
            >
                <InfoIcon />
            </IconButton>
            <Dialog
                fullWidth={true}
                fullScreen={fullScreen}
                open={openModal}
                onClose={handleClose}
                aria-labelledby="responsive-dialog-title"
            >
                <DialogTitle
                    sx={{ display: "flex", justifyContent: "space-between" }}
                    id="responsive-dialog-title">
                    <IconButton color='primary' onClick={handleClose}>
                        <ArrowBackIosIcon />
                    </IconButton>                    
                    <Typography>
                    {pet.author}
                    </Typography>

                    <Typography>
                    {pet.title}
                    </Typography>
                    
                </DialogTitle>
                <DialogContent
                sx={{display:"flex",alignItems:"center"}}>
                                    
                        <img src={`${pet.img}`} width={"100%"} alt="" />
                   
                </DialogContent>
                <DialogActions>

                </DialogActions>
            </Dialog>

        </div>
    )
}

export default PetModal