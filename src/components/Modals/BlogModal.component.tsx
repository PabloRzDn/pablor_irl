import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import {
  Button,
  Grid,
  IconButton,
  Typography,
  DialogContentText,
  Divider,
} from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";
import { useState } from "react";
import { blogData } from "../Blog/Blog.data";

interface blogModal {
  id: number;
  title: string;
  subtitle: string;
}

interface blogModalData {
  id: number | null;
  content: string;
}



const BlogModal = (props: blogModal) => {
  
  const theme = useTheme();

  const fullScreen = useMediaQuery(theme.breakpoints.down("md"));
  const content = blogData.filter(
    (item: blogModalData) => item.id === props.id
  )[0];

  const [openModal, setOpenModal] = useState(false);
  const handleOpen = () => {
    setOpenModal(true);
  };
  const handleClose = () => {
    setOpenModal(false);
  };
  return (
    <div>
      <Button onClick={handleOpen} size="small">
        <Typography variant="body2">Ver Más</Typography>
      </Button>
      <Dialog
        scroll="paper"
        fullWidth={true}
        fullScreen={fullScreen}
        open={openModal}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle
          sx={{ display: "flex", justifyContent: "space-between" }}
          id="responsive-dialog-title"
        >
          <IconButton color="primary" onClick={handleClose}>
            <ArrowBackIosIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <Grid container>
            <Grid item xs={12} xl={12}>
              <Typography mb={2} variant="h5">{props.title}</Typography>
            
              <Typography  mb={2} variant="body1">{props.subtitle}</Typography>
              <Divider></Divider>
              <DialogContentText>
                <Typography style={{ whiteSpace: "pre-line" }} variant="body2">
                  {content.content}
                </Typography>
              </DialogContentText>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    </div>
  );
};

export default BlogModal;
