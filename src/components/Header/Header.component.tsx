import { Avatar, Box, Grid, Typography, Link } from "@mui/material";

import CreateIcon from "@mui/icons-material/Create";
import PrecisionManufacturingIcon from "@mui/icons-material/PrecisionManufacturing";
import TerminalIcon from "@mui/icons-material/Terminal";
import ThunderstormIcon from "@mui/icons-material/Thunderstorm";
import uChile from "./assets/universidad-de-chile-logo-1.png";
import avatar from "./assets/avatar.jpg";
import InstagramIcon from '@mui/icons-material/Instagram';
enum content {
  BORN = "BORN",
  MECH = "MECH",
  SOFTWARE = "SOFTWARE",
  EDUCAZUL = "EDUCAZUL",
  POEMS = "POEMS",
  INSTAGRAM = "INSTAGRAM"
}

const headerContent = [
  {
    id: 1,
    index: content.BORN,
    label: "Julio del 91",
    icon: (
      <ThunderstormIcon
        fontSize="small"
        color="primary"
        sx={{ marginRight: 1 }}
      />
    ),
  },
  {
    id: 2,
    index: content.MECH,
    label: "Ingeniero Mecánico",
    icon: (
      <PrecisionManufacturingIcon
        sx={{ marginRight: 1 }}
        fontSize="small"
        color="primary"
      />
    ),
  },
  {
    id: 3,
    index: content.SOFTWARE,
    label: "Desarrollador de software",
    icon: (
      <TerminalIcon sx={{ marginRight: 1 }} fontSize="small" color="primary" />
    ),
  },
  {
    id: 4,
    index: content.EDUCAZUL,
    label: "Educazul",
    icon: <><img alt="logo-udechile" src={uChile} height={20} color="primary" />&nbsp; </>,
  },
  {
    id: 5,
    index: content.POEMS,
    label: "Leo y escribo también",
    icon: (
      <CreateIcon sx={{ marginRight: 1 }} fontSize="small" color="primary" />
    ),
  },
  {
    id: 6,
    index: content.INSTAGRAM,
    label:  <Link href="https://instagram.com/pablo_ivl/">@pablo_ivl</Link>,
    icon: (
      <InstagramIcon sx={{ marginRight: 1 }} fontSize="small" color="primary" />
    ),
  },
];

const Header = () => {
  return (
    <div>
      <Grid container mb={4}>
        <Grid
          item
          height={"20vh"}
          display={"flex"}
          justifyContent={"center"}
          alignItems={"center"}
          xs={12}
          sm={12}
          md={5}
          lg={5}
          xl={5}
        >
          <Avatar
            src={avatar}
            sx={{ height: { xs: 56, lg: 150 }, width: { xs: 56, lg: 150 } }}
          />
        </Grid>
        <Grid
          item
          display={"flex"}
          justifyContent={"center"}
          xs={12}
          sm={12}
          md={7}
          lg={7}
          xl={7}
        >
          <Box>
            <Grid container>
              <Grid
                item
                display={"flex"}
                justifyContent={{ xs: "center", sm: "center", md: "start" }}
                my={3}
                ml={{ sm: 0, xs: 0, md: 5, lg: 5, xl: 5 }}
                xs={12}
                sm={12}
                md={12}
                lg={12}
                xl={12}
              >
                <Typography variant="h5">Pablo Ruz Donoso</Typography>
              </Grid>
              
              {headerContent.map((item: any) => (
                <Grid
                key={item.id}
                  item
                  display={"flex"}
                  justifyContent={"start"}
                  justifyItems={"center"}
                  ml={5}
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                  xl={12}
                >
                  <Typography
                    variant="body1"
                    display={"flex"}
                    alignItems={"center"}
                  >
                    {item.icon}
                    -{item.label}
                  </Typography>
                </Grid>
              ))}
              
            </Grid>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
};

export default Header;
