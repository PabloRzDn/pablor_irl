import {
  ImageList,
  ImageListItem,
  ImageListItemBar
} from '@mui/material';

import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import WorkModal from '../Modals/WorkModal.component';
import { workData } from './Work.data';
const Work = () => {
    const theme = useTheme();

    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    return (
      
        <ImageList 
        cols={3} 
        rowHeight={fullScreen? 120 : 250}
        variant="quilted" 
        sx={{ width: "100vh", height: "55vh" }}>
          
          {workData.map((item) => (
            <ImageListItem key={item.img}>
              <img
                src={`${item.img}?w=248&fit=crop&auto=format`}
                srcSet={`${item.img}?w=248&fit=crop&auto=format&dpr=2 2x`}
                alt={item.title}
                loading="lazy"
              />
              <ImageListItemBar
                title={item.title}
                subtitle={item?.author}
                actionIcon={
                  <WorkModal work={item}/>
                }
              />
            </ImageListItem>
          ))}
        </ImageList>
    )
}

export default Work