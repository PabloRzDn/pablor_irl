export const workData = [
    {
        id: 1,
        img: require("./assets/work001.png"),
        title: "Placa PCB",
        author: "IoT - Ingeniería",
        description:"Diseño y simulación de placas PCB para distintos elementos electrónicos"
    },
    {
        id: 2,
        img: require("./assets/work002.png"),
        title: "Prototipo IoT",
        author: "IoT - Ingeniería",
        description:"Diseño, impresión 3D y prototipado electrónico de sistemas IoT basado en placas ESP32 y control por relé"
    },
    {
        id: 3,
        img: require("./assets/work003.png"),
        title: "Invernadero IoT",
        author: "IoT - Ingeniería",
        description:"Diseño, impresión 3D y prototipado electrónico de sistemas IoT basado en Raspberry PI y control por relé"

    },
    {
        id: 4,
        img: require("./assets/work004.png"),
        title: "Modelado 3D",
        author: "IoT - Ingeniería",
        description:"Diseño e impresión 3D de cases para prototipos electrónicos"

    },
    {
        id: 5,
        img: require("./assets/work005.jpg"),
        title: "Software RFID",
        author: "IoT - Ingeniería",
        description:"Prototipado electrónico y desarrollo de software de aplicación CRUD utilizando módulos RFID"

    },
    {
        id: 6,
        img: require("./assets/work006.jpeg"),
        title: "Scan huella",
        author: "Impresión 3D",
        description:"Diseño iterativo e  impresión 3D a medida para elementos electrónicos"

    },
    {
        id: 7,
        img: require("./assets/work007.jpeg"),
        title: "Replica Materiales",
        author: "Impresión 3D",
        description:"Diseño, prototipado e impresión 3D para piezas en distintos materiales de fabricación (composito FC y ASA) "

    },
    {
        id: 8,
        img: require("./assets/work008.png"),
        title: "Solar Tracker",
        author: "IoT - Ingeniería",
        description:"Determinación de ecuaciones de trayectoria solar para software de control mecatrónico de seguidor solar."
    },
]