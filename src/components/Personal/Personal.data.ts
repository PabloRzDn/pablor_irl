
export const personalList = [
    {
        id: 1,
        img: require("./assets/personal1.jpg"),
        title: "",
        author: ""
    },
    {
        id: 2,
        img: require("./assets/personal2.jpg"),
        title: "China",
        author: ""
    },
    {
        id: 3,
        img: require("./assets/personal3.jpg"),
        title: "Ruta de la Muerte",
        author: "Bolivia"
    },
    {
        id: 4,
        img: require("./assets/personal4.jpg"),
        title: "",
        author: "Estadio CAP"
    },
    {
        id: 5,
        img: require("./assets/personal5.jpg"),
        title: "Amigui Pame",
        author: ""
    },
    {
        id: 56,
        img: require("./assets/personal56.jpg"),
        title: "Educazul",
        author: ""
    },
    {
        id: 57,
        img: require("./assets/personal57.jpg"),
        title: "Canillitas",
        author: ""
    },
    {
        id: 6,
        img: require("./assets/personal6.jpg"),
        title: "Altiplano",
        author: ""
    },
    {
        id: 7,
        img: require("./assets/personal7.jpg"),
        title: "Altiplano",
        author: ""
    },
    {
        id: 8,
        img: require("./assets/personal8.jpg"),
        title: "",
        author: ""
    },
    {
        id: 9,
        img: require("./assets/personal9.jpg"),
        title: "",
        author: ""
    },
    {
        id: 10,
        img: require("./assets/personal10.jpg"),
        title: "",
        author: ""
    },
    {
        id: 11,
        img: require("./assets/personal11.jpg"),
        title: "",
        author: "Bolivia"
    },
  
    
    {
        id: 15,
        img: require("./assets/personal15.jpg"),
        title: "Tiwanaco",
        author: ""
    },
    

    {
        id: 18,
        img: require("./assets/personal18.jpg"),
        title: "Tiwanaco",
        author: ""
    },
    {
        id: 19,
        img: require("./assets/personal19.jpg"),
        title: "Tiwanaco",
        author: ""
    },
   
    {
        id: 22,
        img: require("./assets/personal22.jpg"),
        title: "Tiwanaco",
        author: ""
    },
    {
        id: 23,
        img: require("./assets/personal23.jpg"),
        title: "Tiwanaco",
        author: ""
    },
    {
        id: 24,
        img: require("./assets/personal24.jpg"),
        title: "Tiwanaco",
        author: ""
    },
    
    {
        id: 26,
        img: require("./assets/personal26.jpg"),
        title: "El Alto",
        author: ""
    },
    
  
    {
        id: 32,
        img: require("./assets/personal32.jpg"),
        title: "Copacabana",
        author: ""
    },
    {
        id: 33,
        img: require("./assets/personal33.jpg"),
        title: "Copacabana",
        author: ""
    },
    
    {
        id: 37,
        img: require("./assets/personal37.jpg"),
        title: "Siles",
        author: ""
    },
    {
        id: 38,
        img: require("./assets/personal38.jpg"),
        title: "Plato Paceño",
        author: ""
    },
    {
        id: 39,
        img: require("./assets/personal39.jpg"),
        title: "Las Brujas",
        author: ""
    },
    {
        id: 40,
        img: require("./assets/personal40.jpg"),
        title: "Los Yungas",
        author: ""
    },
    
    
    {
        id: 43,
        img: require("./assets/personal43.jpg"),
        title: "Los Yungas",
        author: ""
    },
    {
        id: 44,
        img: require("./assets/personal44.jpg"),
        title: "Los Yungas",
        author: ""
    },
    
    {
        id: 47,
        img: require("./assets/personal47.jpg"),
        title: "",
        author: ""
    },
    {
        id: 48,
        img: require("./assets/personal48.jpg"),
        title: "",
        author: ""
    },
    {
        id: 49,
        img: require("./assets/personal49.jpg"),
        title: "Educazul",
        author: ""
    },
   {
        id: 51,
        img: require("./assets/personal51.jpg"),
        title: "",
        author: ""
    },
    
    {
        id: 53,
        img: require("./assets/personal53.jpg"),
        title: "",
        author: ""
    },
   
    {
        id: 55,
        img: require("./assets/personal55.jpg"),
        title: "Karawaira",
        author: ""
    },
    
]