import { Box } from '@mui/material'
import CustomAppBar from '../AppBar/AppBar.component'

const Layout = ({children}:any) => {
  return (
    <Box component={"div"}>
      <CustomAppBar/>
        {children}
    </Box>
  )
}

export default Layout