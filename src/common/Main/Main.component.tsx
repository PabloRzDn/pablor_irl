import { Container } from "@mui/material";
import Header from "../../components/Header/Header.component";
import CustomTabs from "../CustomTabs/CustomTabs.component";
const Main = () => {
  return (
    <div>
      <Container sx={{ marginTop: 3 }}>
        <Header />
        <CustomTabs />
      </Container>
    </div>
  );
};

export default Main;
