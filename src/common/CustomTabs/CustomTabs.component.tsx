import { Box, Tab, Tabs } from "@mui/material";
import React, { useState } from "react";
import Personal from "../../components/Personal/Personal.components";
import Pets from "../../components/Pets/Pets.components";
import Work from "../../components/Work/Work.components";
import Blog from "../../components/Blog/Blog.component";

enum components {
  WORK = "WORK",
  PERSONAL = "PERSONAL",
  PETS = "PETS",
  BLOG = "BLOG",
}

interface tabContentInterface {
  index: components;
  component: any;
  label: string;
}

const tabContent: tabContentInterface[] = [
  { index: components.BLOG, component: <Blog />, label: "Blog" },
  { index: components.PERSONAL, component: <Personal />, label: "Insta" },
  { index: components.PETS, component: <Pets />, label: "Nai & Dora" },
  { index: components.WORK, component: <Work />, label: "Trabajo" },
];

const CustomTabs = () => {
  const [tabIndex, setTabIndex] = useState(components.BLOG);

  const handleTabChange = (
    event: React.SyntheticEvent,
    newValue: components
  ) => {
    setTabIndex(newValue);
  };

  const getTabComponent = (tabIndex: components) => {
    return tabContent.filter((item: tabContentInterface) => item.index === tabIndex)[0]
      .component;
  };
  return (
    <>
      <Box
        display={"flex"}
        justifyContent={"center"}
        sx={{ borderBottom: 2, borderColor: "divider" }}
      >
        <Tabs variant="fullWidth" value={tabIndex} onChange={handleTabChange}>
          {tabContent.map((item: tabContentInterface) => (
            <Tab key={`${item.index}_tab`} value={item.index} label={item.label} />
          ))}
        </Tabs>
      </Box>
      <Box display={"flex"} justifyContent={"center"}>
        {getTabComponent(tabIndex)}
      </Box>
    </>
  );
};

export default CustomTabs;
